#1
select count(customers.CustomerID) as numberOfCustomers, customers.Country 
from customers
group by customers.Country
order by numberOfCustomers desc ;

#2
select count(products.ProductID) as numberOfProducts, suppliers.SupplierID
from products join suppliers on products.SupplierID=suppliers.SupplierID
group by products.SupplierID
order by products desc;


select * from employees;

show variables like 'secure_file_priv';

load data local infile "/Users/turkanarit/Desktop/dbdata/customers.csv" 
 into table customers 
 fields terminated by ';' ignore 1 lines;
 
 load data local infile "/Users/turkanarit/Desktop/dbdata/employees.csv"
 into table employees
 fields terminated by ','
ignore 1 lines;

 load data local infile "/Users/turkanarit/Desktop/dbdata/categories.tsv"
 into table categories
 fields terminated by '\t';

 load data local infile "/Users/turkanarit/Desktop/dbdata/suppliers.tsv"
 into table suppliers
 fields terminated by '\t';
 
load data local infile "/Users/turkanarit/Desktop/dbdata/orderDetails.tsv"
 into table orderdetails
 fields terminated by '\t';
 
  load data local infile "/Users/turkanarit/Desktop/dbdata/orders.csv"
 into table orders
 fields terminated by '\t';
 
load data local infile "/Users/turkanarit/Desktop/dbdata/products.csv"
 into table products
 fields terminated by '\t';

 load data local infile "/Users/turkanarit/Desktop/dbdata/shippers.tsv"
 into table shippers
 fields terminated by '\t';
 
 